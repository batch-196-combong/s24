console.log('Hello Dave');

// ES6 Upates
// the latest major update to Javascript

// Before Update
let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);

// After Update
// Exponent Operators (**)

let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

let fourSquaroot = 4**.5;
console.log(fourSquaroot);

let string1 = "Javasript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";

let sentence1;
let sentence2;

// sentence1 = string1 + ' ' + string3 + ' ' + string2 + ' ' + string5;
// console.log(sentence1);

// sentence2 = string4  + ' ' + string3 + ' ' + string1;
// console.log(sentence2);

sentence1 = `${string1} ${string3} ${string2} ${string5}`;

sentence2 = `${string4} ${string3} ${string1}`;

console.log(sentence1);
console.log(sentence2);

let sentence3 = `${string6} ${string7} ${'Bootcamp'}`
console.log(sentence3);

let sentence4 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence4);

let person = {
	name: 'Michael',
	position: 'developer',
	income: 50000,
	expenses: 60000
}

console.log(`${person.name} is a ${person.position}`)
console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}`);

// Destructuring Arrays and Objects

let array1 = ['Curry', 'Lillard', 'Paul', 'Irving'];

// let player1 = array1[0];
// let player2 = array1[1];
// let player3 = array1[2];
// let player4 = array1[3];


let [player1, player2, player3, player4] = array1;
console.log(player1, player2, player3, player4);

let array2 = ['Jokic', 'Embiid', 'Howard', 'Anthony-Towns'];

let [center1, center2, , center4] = array2;
console.log(center4);

let pokemon1 = {
	name: 'Balbasaur',
	type: 'Grass',
	level: 10,
	moves: ['Razor Leaf', 'Tackle', 'Leech Seed']
}

let {level, type, name, moves, personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality);

let pokemon2 = {
	name: 'Charmander',
	type: 'fire',
	level: 11,
	moves: ['Ember', 'Scratch']
}

let {name: name2} = pokemon2;
console.log(name2);


// Arrow Functions
function displayMsg(){
	console.log('Hello World');
}
displayMsg();

const hello = () => {
	console.log('Hello From Arrow')
};

hello();

const greet = (friend) => {
	console.log(friend);
}
greet(person);

// Arrow vs Function

// Implicit return - allows us to return a value from an arrow function without the use of return keyword. 

// function addNum(num1, num2){
// 	return num1 + num2;
// }



let subNum = (num1,num2) => num1 - num2;
let difference = subNum(10,5);
console.log(difference);

// Implicit return will only work on arrow functions without {};

let addNum = (num1,num2) => num1 + num2;
let sum = addNum(50,70);
console.log(sum);

let character1 = {
	name: 'Cloud Strife',
	occupation: 'Soldier',
	greet: function(){
		console.log(this);
		console.log(`Hi! I am ${this.name}`);
	},

	introduceJob: () => {
		console.log(this);
	}
}

character1.greet();
character1.introduceJob();
//arrow function are not advisable as a method in an object.

// Class Based Objects Blueprints
	

// Constructor Function
	// function Pokemon(name, type, level){
	// 	this.name = name;
	// 	this.type = type;
	// 	this.level = level;
	// }

// Class
	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}
let car1 = new Car('Toyota', 'Vios', '2002');
let car2 = new Car('Cooper', 'Mini', '1969');
let car3 = new Car('Porsche', '911', '1967');

console.log(car1);
console.log(car2);
console.log(car3);


class Pokemon {
	constructor(name, type, level){
		this.name = name;
		this.type = type;
		this.level = level;
	}
}

let pokemonA = new Pokemon('Xerneas', 'Fairy', '111');
let pokemonB = new Pokemon('Mewto', 'Psychic', '109');

console.log(pokemonA);
console.log(pokemonB);
